package xenolith.ftpserver;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    private ForegroundService service;

    private TextView serverStatus, serverAddress;
    private FancyButton button;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.main_button);
        serverStatus = findViewById(R.id.text_server_status_var);
        serverAddress = findViewById(R.id.text_server_status_address);

        intent = new Intent(MainActivity.this, ForegroundService.class);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.moveToState(FancyButton.State.WAITING);
                if (ForegroundService.isRunning()) {
                    intent.setAction(ForegroundService.STOP);
                    startService(intent);
                } else {
                    intent.setAction(ForegroundService.START);
                    startService(intent);
                    bindForegroundService();
                }
            }
        });

        if (ForegroundService.isRunning()) bindForegroundService();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // permission is not granted, and we want it
            button.moveToState(FancyButton.State.WAITING);
            ActivityCompat.requestPermissions(this,
                    new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                    REQUEST_WRITE_EXTERNAL_STORAGE);
        }

        updateUI();
    }

    private void bindForegroundService() {
        bindService(intent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                service = ((ForegroundService.CustomBinder) binder).getService();
                service.setStateListener(new ForegroundService.StateListener() {
                    @Override
                    public void call(boolean running) {
                        updateUI();
                    }
                });
                updateUI();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                updateUI();
            }
        }, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                button.moveToState(FancyButton.State.INACTIVE);
                updateUI();
            } else {
                Log.e(TAG, "Application does not have the necessary permissions to work properly!");
            }
        }
    }

    private static class RetrieveLocalAddress extends AsyncTask<Void, Void, String> {
        private Exception exception;
        private WeakReference<MainActivity> activityReference;

        RetrieveLocalAddress(MainActivity activity) {
            this.activityReference = new WeakReference<>(activity);
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                MainActivity activity = activityReference.get();
                if (activity == null || activity.isFinishing()) return null;

                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                    NetworkInterface intf = en.nextElement();
                    if (intf.getName().contains("wlan") || intf.getName().contains("ap")) {
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
                                .hasMoreElements();) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress()
                                    && (inetAddress.getAddress().length == 4)) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                exception = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String address) {
            if (exception != null) {
                Log.e(TAG, "Cannot get the localhost address!", exception);
            }
            if (address != null) {
                MainActivity activity = activityReference.get();
                if (activity == null || activity.isFinishing()) return;
                activity.serverAddress.setText(activity.getString(R.string.text_server_status_address_template,
                        address, Config.PORT));
                Log.d(TAG, "Local IP address: " + address);
            } else {
                Log.e(TAG, "Cannot get the localhost address for some reason!");
            }
        }
    }

    private void updateUI() {
        if (ForegroundService.isRunning()) {
            serverStatus.setText(R.string.text_server_status_active);
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorActive2));
            }
            new RetrieveLocalAddress(this).execute();
            button.moveToState(FancyButton.State.ACTIVE);
            Log.d(TAG, "Init UI: Service ACTIVE");
        } else {
            serverStatus.setText(R.string.text_server_status_inactive);
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorInactive2));
            }
            button.moveToState(FancyButton.State.INACTIVE);
            Log.d(TAG, "Init UI: Service INACTIVE");
        }
    }
}
