package xenolith.ftpserver;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

public class FancyButton extends FrameLayout {
    private static final long ANIMATION_DURATION = 500;

    public enum State {
        ACTIVE, WAITING, INACTIVE
    }

    private ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);

    private State oldState = State.INACTIVE, currentState = State.INACTIVE;

    private View iconPlay;
    private View iconStop;
    private View backActive;
    private View backInactive;
    private View frontWaiting;

    public FancyButton(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.component_fancy_button, this);
        }

        animator.setDuration(ANIMATION_DURATION);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                updateView(oldState, currentState, animation.getAnimatedFraction());
            }
        });
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        iconPlay = findViewById(R.id.fancy_button_play_icon);
        iconStop = findViewById(R.id.fancy_button_stop_icon);
        backActive = findViewById(R.id.fancy_button_active_back);
        backInactive = findViewById(R.id.fancy_button_inactive_back);
        frontWaiting = findViewById(R.id.fancy_button_waiting);
    }

    /**
     * Sets a new state instantly (without transitions).
     */
    public void setCurrentState(State state) {
        oldState = state;
        currentState = state;
        updateView(oldState, currentState, 1.0f);
    }

    /**
     * Sets a new state (with transition animation).
     */
    public void moveToState(State state) {
        oldState = currentState;
        currentState = state;
        animator.start();
    }

    /**
     * Calculate new button look, based on transition percent.
     * @param percent value from 0.0f to 1.0f, where 0 is the beginning of transition and 1.0 - complete transition
     */
    private void updateView(State oldState, State currentState, float percent) {
        updateAlpha(iconPlay, State.INACTIVE, percent);
        updateAlpha(iconStop, State.ACTIVE, percent);
        updateAlpha(backActive, State.ACTIVE, 0.5f + percent / 2.0f);
        updateAlpha(backInactive, State.INACTIVE, 0.5f + percent / 2.0f);
        updateAlpha(frontWaiting, State.WAITING, percent / 2.0f);
    }

    private void updateAlpha(View view, State state, float percent) {
        if (oldState == state) view.setAlpha(1.0f - percent);
        if (currentState == state) view.setAlpha(percent);
        else view.setAlpha(0);
    }
}
