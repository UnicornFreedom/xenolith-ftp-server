package xenolith.ftpserver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;

public class ForegroundService extends Service {
    class CustomBinder extends Binder {
        ForegroundService getService() {
            return ForegroundService.this;
        }
    }

    interface StateListener {
        void call(boolean running);
    }

    private final static int ID = 1334235;
    private final static String TAG = "ForegroundService";
    private final static String NOTIFICATION_CHANNEL_ID = "xenolith.ftpserver.notifications";
    private final static String NOTIFICATION_CHANNEL_NAME = "Xenolith FTP Server";

    public final static String START = "start";
    public final static String STOP = "stop";

    private NotificationManager manager;
    private CustomBinder binder = new CustomBinder();
    private StateListener listener;

    private FtpServer server;

    private static boolean running = false;

    public static boolean isRunning() { return running; }

    public void setRunning(boolean running) {
        ForegroundService.running = running;
        if (listener != null) {
            listener.call(running);
        }
    }

    public void setStateListener(StateListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action != null) {
            switch (action) {
                case START:
                    startForeground(ID, createNotification());
                    startFTPServer();
                    setRunning(true);
                    Log.d(TAG, "Service started...");
                    break;
                case STOP:
                    stopFtpServer();
                    stopForeground(true);
                    stopSelf();
                    setRunning(false);
                    Log.d(TAG, "Service stopped...");
                    break;
                default:
            }
        }
        return START_STICKY;
    }

    private NotificationManager getManager() {
        if (manager == null) {
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }

    private Notification createNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle(getString(R.string.text_notification_title))
                .setContentText(getString(R.string.text_notification_message))
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentIntent(pendingIntent)
                .setTicker(getString(R.string.text_notification_title));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_LOW);
            getManager().createNotificationChannel(channel);
            return builder.setChannelId(NOTIFICATION_CHANNEL_ID).build();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return builder.build();
        } else {
            //noinspection deprecation
            return builder.getNotification();
        }
    }

    private void startFTPServer() {
        try {
            FtpServerFactory serverFactory = new FtpServerFactory();
            ListenerFactory listenerFactory = new ListenerFactory();
            listenerFactory.setPort(Config.PORT);
            serverFactory.addListener("default", listenerFactory.createListener());
            serverFactory.setUserManager(new CustomUserManager("/storage/emulated/0/"));
            server = serverFactory.createServer();
            server.start();
        } catch (FtpException e) {
            Log.e(TAG, "Cannot start FTP server!", e);
        }
    }

    private void stopFtpServer() {
        server.stop();
    }
}
