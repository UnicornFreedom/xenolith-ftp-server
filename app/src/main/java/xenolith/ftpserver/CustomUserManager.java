package xenolith.ftpserver;

import android.os.Environment;

import org.apache.ftpserver.ftplet.Authentication;
import org.apache.ftpserver.ftplet.AuthenticationFailedException;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.usermanager.AnonymousAuthentication;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.apache.ftpserver.usermanager.impl.BaseUser;

import java.util.HashMap;

public class CustomUserManager implements UserManager {
    private HashMap<String, User> users = new HashMap<>();
    private BaseUser anonymous = new BaseUser();
    private BaseUser admin = new BaseUser();

    CustomUserManager(String homeDirectory) {
        anonymous.setName("anonymous");
        anonymous.setPassword("anonymous");
        anonymous.setHomeDirectory(homeDirectory);

        admin.setName("admin");
        admin.setPassword("admin");
        admin.setHomeDirectory(homeDirectory);
    }

    @Override
    public User getUserByName(String username) {
        return users.get(username);
    }

    @Override
    public String[] getAllUserNames() {
        return users.keySet().toArray(new String[0]);
    }

    @Override
    public void delete(String username) {
        users.remove(username);
    }

    @Override
    public void save(User user) {
        users.put(user.getName(), user);
    }

    @Override
    public boolean doesExist(String username) {
        return users.containsKey(username);
    }

    @Override
    public User authenticate(Authentication authentication) throws AuthenticationFailedException {
        if (authentication instanceof AnonymousAuthentication) {
            return anonymous;
        } else {
            UsernamePasswordAuthentication auth = (UsernamePasswordAuthentication) authentication;
            User user = users.get(auth.getUsername());
            if (user == null) throw new AuthenticationFailedException("Incorrect username!");
            if (!user.getPassword().equals(auth.getPassword()))
                throw new AuthenticationFailedException("Incorrect password!");
            return user;
        }
    }

    @Override
    public String getAdminName() {
        return admin.getName();
    }

    @Override
    public boolean isAdmin(String username) {
        return username.equals(admin.getName());
    }
}
