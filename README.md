## Xenolith FTP Server
Ultra minimalistic FTP server app for Android.

![](https://i.imgur.com/pyaQiy1.png)

May be useful for file management.  
For example, upload a music album to your phone or download a bunch of photos from it.
